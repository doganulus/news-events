---
type: news
title: Three Medals to Boğaziçi Students in International Mathematical Olimpiad 2021
date: 2022-01-27
image: /images/uploads/entertaiment-01.jpg
---
Ömer Faruk Erzurumluoğlu, Yusuf Kağan Çiçekdağ, and Hayrettin Eren Yıldız from Boğaziçi University Computer Engineering Department received medals at International Mathematical Olimpiad 2021. 