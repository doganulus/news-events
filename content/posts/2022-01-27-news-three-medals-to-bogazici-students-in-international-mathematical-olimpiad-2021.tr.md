---
type: news
title: Boğaziçili Üç Öğrenciye Uluslararası Matematik Olimpiyatları’nda Madalya
date: 2022-01-27
image: /images/uploads/entertaiment-01.jpg
---
Boğaziçi Üniversitesi Bilgisayar Mühendisliği Bölümü’nden başarılı üç öğrenci, Uluslararası Matematik Olimpiyatları’nda (IMO) iki bronz ve bir gümüş madalya kazandı. Lise dönemleri boyunca ulusal ve uluslararası olimpiyatlarda ter döken Ömer Faruk Erzurumoğlu, Yusuf Kağan Çiçekdağ ve Hayrettin Eren Yıldız, olimpiyat çalışmalarının Boğaziçi Üniversitesi tercih etmelerinde çok etkisi olduğunu belirtiyor.

Dünyanın alanında en zorluları arasında gösterilen Uluslararası Matematik Olimpiyatları’nda (IMO) bu akademik yıl Boğaziçi Üniversitesi Bilgisayar Mühendisliği lisans programına yerleşen üç öğrenci büyük başarı elde etti. Bu sene dünya çapından yaklaşık 120 ülkeden altışar kişilik milli takımların yer aldığı IMO'da, Türk Milli Takımı’ndan Yusuf Kağan Çiçekdağ ile Hayrettin Eren Yıldız bronz, Ömer Faruk Erzurumoğlu ise gümüş madalya kazandı. Lise dönemleri ulusal ve uluslararası matematik olimpiyatlarına yoğun hazırlık süreçleriyle geçen başarılı genç Boğaziçililer, bu yıl bilgisayar mühendisliği programına yerleşseler de matematikle çift ana dal yapmak istediklerini dile getiriyor.

“İKİNCİ SORU ZORLADI”

Bronz madalya sahibi Hayrettin Eren Yıldız, bu yılki olimpiyatlarda soruların zorluk derecesinin bekledikleri gibi olmadığını ama buna rağmen ellerinden gelenin en iyisini yaptıklarını söylüyor. Başarılı öğrenci, “Bu yıl hazırlanan sorular beklediğimiz gibi değildi. Genelde düzenlenen sınavda altı soru yer alır ve bunların zorluk derecesi gittikçe artar. Ancak bu sefer ikinci sorunun zor olması bizim dengemizi etkiledi. Bu soruya çok zaman harcamak zorunda kaldık ve bu da bizi biraz zorladı. Ama kendimizi toparlayarak elimizden gelenin en iyisini yaptık” diye konuşuyor.

“IMO MATEMATİĞİN ŞAMPİYONLAR LİGİ”

Gümüş madalya almaya hak kazanan Ömer Faruk Erzurumoğlu ise takım arkadaşlarına göre biraz daha iyi bir performans sergilediğini ifade ediyor. Uluslararası Matematik Olimpiyatları’nda geleneksel bir sıralama olmadığını anlatarak, IMO’nun derecelendirme sistemi şöyle özetliyor:

“Olimpiyatlarda bir, iki ya da üçüncü gibi sıralamalar yer almıyor. Yaklaşık 5 saat süren iki aşamalı sınavda elde ettiğiniz puana göre bronz, gümüş ve altın madalyalar dağıtılıyor. Ancak her ülkeden lise düzeyinde en iyiler buraya gelebiliyor. Ülkelerde mili takımlara girecek en iyi altıların belirlenmesi gerekiyor ve bunun için de birçok ulusal yarışmayı başarıyla tamamlamanız bekleniyor. IMO'yu futbolda Şampiyonlar Ligi'ne benzetebiliriz ve burada başarılı olmak için gerçekten iyi hazırlanmalısınız. Çünkü Çin gibi rekabetçi ülkelerden üst düzey ekiplerle yarışıyorsunuz."

“BOĞAZİÇİ OLİMPİYATÇILARIN GÖZDESİ”

Bu yıl Boğaziçi Üniversitesi Bilgisayar Mühendisliği lisans programına yerleşen öğrenciler, matematikle çift ana dal yapmayı hedeflediklerini de dile getiriyor. Yusuf Kağan Çiçekdağ olimpiyata hazırlanmak için uzun yıllar yoğun matematik çalıştığını ve bu alandan kolay kopamayacağını söylüyor. Olimpiyatlar için zorlu hazırlık sürecinin Boğaziçi Üniversitesi’ni tercih etmelerinde etkisini ise, “Boğaziçi, biz olimpiyatçıların en çok tercih ettiği üniversite. Eğitim kalitesiyle her zaman ön plana çıkıyor. Kampüste zaman zaman diğer alanlardaki milli takımlarda yer almış arkadaşlarımızla karşılaşıyoruz. Yani sadece matematik değil diğer alanlarda da Boğaziçi’nin gözde olduğunu söyleyebilirim” sözleriyle anlatıyor.

Fotoğraftakiler (Soldan sağa): Yusuf Kağan Çiçekdağ, Hayrettin Eren Yıldız, Ömer Faruk Erzurumoğlu